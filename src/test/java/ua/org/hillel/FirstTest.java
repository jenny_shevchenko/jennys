package ua.org.hillel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

// import org.testng.Assert;

public class FirstTest {
    WebDriver driver;

    @BeforeTest
    public void preconditions(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void firstTest() throws InterruptedException {

        driver.get("https://ithillel.ua/");
        driver.findElement(By.xpath("//div[@class='wrap_cityTabs']//a[@href='https://kharkiv.ithillel.ua/']")).click();
        driver.findElement(By.xpath("//li//a[@href='https://kharkiv.ithillel.ua/courses']")).click();
        driver.findElement(By.xpath("//a[contains(@href,'/qa-automation-kharkiv')]//div[@class='text_details' and contains(text(),'Детали курса')]")).click();
        driver.findElement(By.xpath("//form[@id='form-subscribe']//input[@placeholder='Ваш e-mail']")).sendKeys("ekaterinanikon1993@gmail.com");
        driver.findElement(By.xpath("//form[@id='form-subscribe']//input")).submit();
        driver.findElement(By.xpath("//div[contains(text(),'Вы успешно подписались')]")).isDisplayed();

//       /* Assert.assertTrue(driver.findElement(By.xpath("//div[contains(text(),'Вы успешно подписались')]")).isDisplayed());
//        assertThat("Pop up is not present", driver.findElement(By.xpath("//div[contains(text(),'Вы успешно подписались')]")).isDisplayed());
//*/
        assertThat(driver.findElement(By.xpath("//div[contains(text(),'Вы успешно подписались')]")).isDisplayed());
    }


        @AfterTest
        public void finishTests(){
            driver.close();
            driver.quit();

        }
    }

